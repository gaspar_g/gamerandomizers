from django import forms


class GrandWesternTrailForm(forms.Form):
    VERSION_CHOICES = [('vanilla', 'Vanilla'),
                       ('vanilla+', 'Vanilla+'),
                       ('rttn_addon', 'Rails to the North')]
    PLAYER_CHOICES = [("Red", "Red"),
                      ("Blue", "Blue"),
                      ("Yellow", "Yellow"),
                      ("White", "White")]

    players = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                        choices=PLAYER_CHOICES, required=False)
    game_version = forms.ChoiceField(choices=VERSION_CHOICES, required=False)

