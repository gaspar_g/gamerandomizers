from django.shortcuts import render

from itertools import zip_longest

from randomizers.forms import GrandWesternTrailForm
from randomizers.lib.five_tribes_randomizer import FiveTribes
from randomizers.lib.great_western_trail_randomizer import GrandWesternTrail
from randomizers.lib.helpers import setup_counter
from randomizers.lib.puerto_rico_randomizer import PuertoRico


def index(request):
    return render(request, 'randomizers/index.html')


def five_tribes(request):
    cookie_name = '5t'
    counter = setup_counter(request, cookie_name)
    if request.POST:
        ft = FiveTribes()
        ft.run_randomizer()
        all_lines = ft.all_lines
        return render(request, 'randomizers/five_tribes.html',
                      {'all_lines': all_lines, 'counter': counter})
    else:
        return render(request, 'randomizers/five_tribes.html')


def great_western_trail(request):
    cookie_name = 'GWT'
    counter = setup_counter(request, cookie_name)
    if request.POST:
        form = GrandWesternTrailForm(request.POST)
        if form.is_valid():
            game_version = request.POST.get('game_version')
            players = form.cleaned_data.get('players')
            gwt = GrandWesternTrail(game_version, players)
            gwt.randomize_game()
            first_player = gwt.first_player
            objectives = gwt.players_objectives
            print(objectives)
            player_buildings = gwt.randomized_player_buildings
            main_buildings = gwt.randomized_main_buildings
            buildings = [(building_id, building_side, main_building)
                         for (building_id, building_side), main_building
                         in zip_longest(player_buildings, main_buildings)]

            station_tiles = gwt.randomized_station_tiles

            bonus_tiles = gwt.randomized_bonus_tiles
            return render(request, 'randomizers/great_western_trail.html',
                          {'form': form,
                           'buildings': buildings,
                           'first_player': first_player,
                           'objectives': objectives,
                           'station_tiles': station_tiles,
                           'bonus_tiles': bonus_tiles,
                           'counter': counter})
        else:
            return render(request, 'randomizers/great_western_trail.html',
                          {'form': form})
    else:
        form = GrandWesternTrailForm(request.POST)
        return render(request, 'randomizers/great_western_trail.html',
                      {'form': form})


def puerto_rico(request):
    cookie_name = 'PR'
    counter = setup_counter(request, cookie_name)

    if request.POST:
        pr = PuertoRico()
        pr.run_randomizer()
        buildings = pr.buildings
        return render(request, 'randomizers/puerto_rico.html', {'randomized_player_buildings': buildings,
                                                                'counter': counter})
    else:
        return render(request, 'randomizers/puerto_rico.html')
