import random
from typing import NamedTuple

# Puerto Rico randomized_player_buildings (+ cost in db):
PR_BUILDINGS_FOR_1_DB = ['Small Market', 'Aqueduct']
PR_BUILDINGS_FOR_2_DB = ['Construction Hut', 'Black Market', 'Forest House', 'Hacienda']
PR_BUILDINGS_FOR_3_DB = ['Storehouse', 'Small Warehouse']
PR_BUILDINGS_FOR_4_DB = ['Guesthouse', 'Hospice']
PR_BUILDINGS_FOR_5_DB = ['Office', 'Trading Post', 'Church', 'Large Market']
PR_BUILDINGS_FOR_6_DB = ['Large Warehouse', 'Small Wharf']
PR_BUILDINGS_FOR_7_DB = ['Lighthouse', 'University']
PR_BUILDINGS_FOR_8_DB = ['Library', 'Harbor', 'Specialty Factory', 'Factory']
PR_BUILDINGS_FOR_9_DB = ['Wharf', 'Union Hall']
PR_BUILDINGS_FOR_10_DB = ['Guild Hall', 'Customs House', 'Residence', 'City Hall', 'Fortress',
                          'Cloister', 'Statue']


class DrawnBuildings(NamedTuple):
    cost: int
    names: str


class PuertoRico:

    def __init__(self):
        self.buildings = []
        self._randomized_buildings = []

    def run_randomizer(self):
        self._draw_buildings()
        self._prepare_drawn_buildings_to_present()

    def _draw_buildings(self):
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_1_DB)])
        self._randomized_buildings.append(random.sample(PR_BUILDINGS_FOR_2_DB, 2))
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_3_DB)])
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_4_DB)])
        self._randomized_buildings.append(self._draw_5db_buildings())
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_6_DB)])
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_7_DB)])
        self._randomized_buildings.append(random.sample(PR_BUILDINGS_FOR_8_DB, 2))
        self._randomized_buildings.append([random.choice(PR_BUILDINGS_FOR_9_DB)])
        self._randomized_buildings.append(random.sample(PR_BUILDINGS_FOR_10_DB, 5))

    def _prepare_drawn_buildings_to_present(self):
        for cost, buildings in enumerate(self._randomized_buildings, start=1):
            self.buildings.append(DrawnBuildings(cost, ", ".join(buildings)))

    @staticmethod
    def _draw_5db_buildings():
        # Office & Trading Post can't be together in one game.
        randomized_5_db_buildings = random.sample(PR_BUILDINGS_FOR_5_DB, 2)
        if ('Office' in randomized_5_db_buildings
                and 'Trading Post' in randomized_5_db_buildings):
            randomized_5_db_buildings[1] = random.choice(PR_BUILDINGS_FOR_5_DB[2:])
        return randomized_5_db_buildings
