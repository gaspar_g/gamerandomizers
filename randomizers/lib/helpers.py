def setup_counter(request, cookie_name):
    request.session.set_expiry(86400)
    if request.session.get(cookie_name):
        counter = int(request.session.get(cookie_name)) + 1
        request.session[cookie_name] = counter
        return counter - 1
    else:
        request.session[cookie_name] = '1'
        return 1
