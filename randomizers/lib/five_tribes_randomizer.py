import random

STATIC_PART_OF_IMAGE_PATH = '/randomizers/images/image_'
FILE_EXTENSION = '.png'

NUMBER_OF_TILES = 42
NUMBER_OF_TILES_IN_MIDDLE = 20

TILES = {f'tile_{num}': f'{STATIC_PART_OF_IMAGE_PATH}{num}{FILE_EXTENSION}'
         for num, path in enumerate(range(NUMBER_OF_TILES), start=1)}


class FiveTribes:

    def __init__(self):
        self.all_lines = tuple()
        self._red_line_1 = list()
        self._red_line_2 = list()
        self._red_line_3 = list()
        self._red_line_4 = list()
        self._red_tiles = list()
        self._all_tiles = list()
        self._tiles_1_to_30 = list()
        self._tiles_31_to_36 = list()
        self._tiles_37_to_42 = list()
        self._tiles_in_middle = list()
        self._other_tiles = list()

    def run_randomizer(self):
        self._complete_tiles()
        self._create_pack_of_tiles_to_draw_middle_part()
        self._draw_middle_tiles()
        self._prepare_red_tiles_path()
        self._prepare_red_lines()
        self._randomize_other_tiles()
        self._create_lines()

    def _create_lines(self):
        # Horizontal lines
        line_1 = self._other_tiles[0:7]
        line_2 = self._other_tiles[7:8] + self._red_line_1 + self._other_tiles[8:9]
        line_3 = self._other_tiles[9:10] + self._red_line_2 + self._other_tiles[10:11]
        line_4 = self._other_tiles[11:12] + self._red_line_3 + self._other_tiles[12:13]
        line_5 = self._other_tiles[13:14] + self._red_line_4 + self._other_tiles[14:15]
        line_6 = self._other_tiles[15:22]
        self.all_lines = (line_1, line_2, line_3, line_4, line_5, line_6)

    def _randomize_other_tiles(self):
        self._other_tiles = [tile for tile in self._all_tiles if tile not in self._tiles_in_middle]
        random.shuffle(self._other_tiles)

    def _prepare_red_lines(self):
        self._red_line_1 = self._red_tiles[0:5]
        self._red_line_2 = self._red_tiles[5:10]
        self._red_line_3 = self._red_tiles[10:15]
        self._red_line_4 = self._red_tiles[15:20]

    def _prepare_red_tiles_path(self):
        self._red_tiles = ["".join((tile[:-4], '_red', FILE_EXTENSION))
                           for tile in self._tiles_in_middle]

    def _draw_middle_tiles(self):
        self._tiles_in_middle = random.sample(self._create_pack_of_tiles_to_draw_middle_part(),
                                              NUMBER_OF_TILES_IN_MIDDLE)

    def _create_pack_of_tiles_to_draw_middle_part(self):
        pack_a = self._tiles_1_to_30 + self._tiles_37_to_42
        pack_b = random.sample(pack_a, 14)
        all_middle_tiles = pack_b + self._tiles_31_to_36
        return all_middle_tiles

    def _complete_tiles(self):
        self._all_tiles = [value for tile, value in TILES.items()]
        self._tiles_1_to_30 = self._all_tiles[:30]
        self._tiles_31_to_36 = self._all_tiles[30:36]
        self._tiles_37_to_42 = self._all_tiles[36:42]
