import random
from typing import NamedTuple


STATION_TILES = ['1C / TT:3', '1C / 2Z:3', '1C / 2C:3', '2$ / P:1', 'Z/T / 2G:3',  # Vanilla (5/5)
                 '2C / 0', '$12 / 0', '+2c / B:2', '1C / 2S:3',  # Vanilla+ (9/9)
                 'F / 2W:5', 'W / K:2',  # RTTN Addon (11/11)
                 ]
BONUS_TILES = ["5$ / K3", "P+2", "2X", "L3", "B"]  # RTTN Addon

OBJECTIVES = ['TnZZ', 'BBZ', '34B', 'SSTz']


class GWTBuilding(NamedTuple):
    id: int
    side: str

    def __str__(self):
        return f'Budynek: {self.id} Strona: {self.side}'


class GrandWesternTrail:

    def __init__(self, version, players):
        self.version = version
        self.players = players
        self.players_objectives = []
        self.randomized_player_buildings = []
        self.randomized_main_buildings = []
        self.randomized_station_tiles = []
        self.randomized_bonus_tiles = []
        self.first_player = ''

    def randomize_game(self):
        self.randomize_order_of_movement()
        self.randomize_objectives()
        self.randomize_player_buildings()
        self.randomize_main_buildings()
        self.randomize_station_tiles()
        self.randomize_bonus_tiles()

    def randomize_order_of_movement(self):
        if self.players:
            random.shuffle(self.players)
            self.first_player = random.choice(self.players)

    def randomize_objectives(self):
        objectives = random.sample(OBJECTIVES, len(self.players))
        players_and_objectives = [(player, objective) for player, objective
                                       in zip(self.players, objectives)]
        self.players_objectives.extend(players_and_objectives)

    def randomize_player_buildings(self):
        if self.version == 'vanilla':
            for building_id in range(1, 11):
                side = random.choice(['A', 'B'])
                self.randomized_player_buildings.append(GWTBuilding(id=building_id, side=side))
        elif self.version == 'vanilla+':
            for building_id in range(1, 12):
                side = random.choice(['A', 'B'])
                self.randomized_player_buildings.append(GWTBuilding(id=building_id, side=side))
        elif self.version == 'rttn_addon':
            for building_id in range(1, 13):
                side = random.choice(['A', 'B'])
                self.randomized_player_buildings.append(GWTBuilding(id=building_id, side=side))

    def randomize_main_buildings(self):
        main_buildings = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        random.shuffle(main_buildings)
        self.randomized_main_buildings = main_buildings

    def randomize_station_tiles(self):
        vanilla_tiles = 5       # 5/9
        vanilla_plus_tiles = 9  # 9/11
        # RFTN Addon - 11/11

        station_tiles = STATION_TILES[::]
        if self.version == 'vanilla':
            randomized_station_tiles = [
                (tile_id, tile)
                for tile_id, tile
                in enumerate(random.sample(station_tiles[:vanilla_tiles], 5), start=1)]
            self.randomized_station_tiles = [('', randomized_station_tiles)]
        elif self.version == 'vanilla+':
            randomized_station_tiles = [
                (tile_id, tile)
                for tile_id, tile
                in enumerate(random.sample(station_tiles[:vanilla_plus_tiles], 5), start=1)]
            self.randomized_station_tiles = [('', randomized_station_tiles)]
        else:
            """Addon's draw has 3 parts. A) 5 of 11; B) 2 out of left 6; C) Last 4"""
            part_a = [(tile_id, tile)
                      for tile_id, tile
                       in enumerate(random.sample(station_tiles, 5), start=1)]

            left_tiles = [tile for tile in station_tiles
                            if tile not in [i[1] for i in part_a]]
            part_b = [(tile_id, tile)
                      for tile_id, tile
                      in enumerate(random.sample(left_tiles, 2), start=1)]

            left_tiles = [tile for tile in left_tiles
                          if tile not in [tile[1] for tile in part_b]]

            # Last part should be without tile_id
            part_c = [('', tile)
                      for tile_id, tile
                      in enumerate(left_tiles, start=1)]

            self.randomized_station_tiles = [('Main board', part_a),
                                             ('Overlay', part_b),
                                             ('New York', part_c)]

    def randomize_bonus_tiles(self):
        """Each tile appears twice. We draw 6 out of 10."""
        tiles = BONUS_TILES[::]
        bonus_tiles = 2 * tiles
        if self.version == 'rttn_addon':
            random.shuffle(bonus_tiles)
            self.randomized_bonus_tiles = [(tile_id, tile) for tile_id, tile
                                           in enumerate(random.sample(bonus_tiles, 6), start=1)]