from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('five_tribes/', views.five_tribes, name="five_tribes"),
    path('puerto_rico/', views.puerto_rico, name="puerto_rico"),
    path('great_western_trail/', views.great_western_trail, name="great_western_trail")
]
